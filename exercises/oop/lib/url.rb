# frozen_string_literal: true

require 'uri'
require 'forwardable'

# BEGIN
class Url
  extend Forwardable

  include Comparable

  attr_reader :uri

  def initialize(query)
    @uri = URI(query)
    @params = prepare_query_params(uri.query)
  end

  def_delegator :uri, :scheme, :scheme
  def_delegator :uri, :host, :host

  def query_params
    @params
  end

  def query_param(key, default = nil)
    @params[key] || default
  end

  def <=>(other)
    uri <=> other.uri
  end

  private

  def prepare_query_params(query)
    if query
      query.split('&')
           .each_with_object({}) do |pair, acc|
             key, value = pair.split('=')
             acc[key.to_sym] = value
           end
    else
      {}
    end
  end
end
# END
