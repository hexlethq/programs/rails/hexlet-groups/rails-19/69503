# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def setup
    @stack = Stack.new
  end

  def test_empty_stack
    assert_equal [], @stack.to_a
    assert_equal 0, @stack.size
    assert @stack.empty?
  end

  def test_push
    @stack.push! 'ruby'
    @stack.push! 'php'
    @stack.push! 'java'

    assert_equal %w[ruby php java], @stack.to_a
    assert_equal 3, @stack.size
    refute @stack.empty?
  end

  def test_pop_from_not_empty_
    @stack.push! 'ruby'
    @stack.push! 'php'
    @stack.push! 'java'

    assert_equal %w[ruby php java], @stack.to_a

    @stack.pop!

    assert_equal %w[ruby php], @stack.to_a
    assert_equal 2, @stack.size
    refute @stack.empty?
  end

  def test_pop_from_empty_stack
    @stack.pop!

    assert_equal [], @stack.to_a
    assert_equal 0, @stack.size
    assert @stack.empty?
  end

  def test_clear_stack
    @stack.push! 'ruby'

    assert_equal ['ruby'], @stack.to_a
    assert_equal 1, @stack.size
    refute @stack.empty?

    @stack.clear!

    assert_equal [], @stack.to_a
    assert_equal 0, @stack.size
    assert @stack.empty?
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
