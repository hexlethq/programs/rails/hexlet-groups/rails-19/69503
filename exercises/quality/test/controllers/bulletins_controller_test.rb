require 'test_helper'

class BulletinsControllerTest < ActionDispatch::IntegrationTest
  self.use_transactional_tests = true

  test 'open bulletins list page' do
    get bulletins_path
    
    assert_response :success
  end

  test 'open bulletin page' do
    get bulletin_path(bulletins(:published))
    
    assert_response :success
  end
end