# frozen_string_literal: true

def make_censored(text, stop_words)
  # BEGIN
  words = text.split

  censored_words = words.map do |word|
    stop_words.include?(word) ? '$#%!' : word
  end

  censored_words.join(' ')
  # END
end
