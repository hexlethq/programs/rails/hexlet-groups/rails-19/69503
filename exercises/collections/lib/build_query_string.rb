# frozen_string_literal: true

# BEGIN
def build_query_string(params)
  result = []

  sorted_params = params.sort.to_h

  sorted_params.each_pair do |key, value|
    result << "#{key}=#{value}"
  end

  result.join('&')
end
# END
