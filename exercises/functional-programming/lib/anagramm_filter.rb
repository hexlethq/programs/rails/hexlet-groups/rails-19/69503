# frozen_string_literal: true

# BEGIN
def anagramm_filter(word, searchable_words)
  sorted_word_chars = word.chars.sort

  searchable_words.select { |searchable_word| searchable_word.chars.sort == sorted_word_chars }
end
# END
