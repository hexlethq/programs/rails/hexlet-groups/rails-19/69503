# frozen_string_literal: true

# BEGIN
def get_same_parity(numbers)
  head, = numbers
  numbers.select { |number| head % 2 == number % 2 }
end
# END
