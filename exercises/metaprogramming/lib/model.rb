# frozen_string_literal: true

module Model
  module ClassMethods
    attr_reader :scheme

    def attribute(name, options = {})
      @scheme ||= {}
      @scheme[name] = options

      define_method(name) { @attributes[name] }
      define_method "#{name}=" do |value|
        @attributes[name] = self.class.convert(value, options[:type])
      end
    end

    def convert(value, type)
      return nil if value.nil?

      case type
      when :string
        value.to_s
      when :integer
        value.to_i
      when :boolean
        ['yes', true].include? value
      when :datetime
        DateTime.parse(value)
      end
    end
  end

  def self.included(base)
    base.extend(ClassMethods)
  end

  def initialize(attributes)
    @attributes = self.class.scheme.each_with_object({}) do |(field_name, field_info), acc|
      acc[field_name] = self.class.convert(attributes[field_name], field_info[:type])
    end
  end

  def attributes
    @attributes
  end
end
