# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'home#index'

  get '/page/:id', to: 'pages#show', as: 'page'
end
