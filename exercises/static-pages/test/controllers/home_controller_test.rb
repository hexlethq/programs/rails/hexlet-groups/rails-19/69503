# frozen_string_literal: true

require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  test 'link about me is existing' do
    get '/'
    assert_response :success
    assert_select 'a[href=?]', '/page/about', 'Обо мне'
  end
end
