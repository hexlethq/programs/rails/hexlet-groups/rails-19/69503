# frozen_string_literal: true

require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  test 'about page' do
    get page_path(:about)
    assert_response :success
    assert_select 'a[href=?]', '/', 'Home'
    assert_select 'h1', 'Илона Маскова'
  end
end
