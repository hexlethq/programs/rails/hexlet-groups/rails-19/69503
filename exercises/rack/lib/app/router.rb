# frozen_string_literal: true

module App
  class Router
    def call(_env)
      [404, {}, '404 Not Found']
    end
  end
end
