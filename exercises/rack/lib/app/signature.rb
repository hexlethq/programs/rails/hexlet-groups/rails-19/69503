# frozen_string_literal: true

require 'digest'

module App
  class Signature
    def initialize(app)
      @app = app
    end

    def call(env)
      request = Rack::Request.new(env)

      if ['/', '/about'].include?(request.path)
        body = case request.path
               when '/'
                 'Hello, World!'
               when '/about'
                 'About page'
               end

        signature = Digest::SHA256.hexdigest body

        [200, {}, "#{body}\n#{signature}"]
      else
        @app.call(env)
      end
    end
  end
end
