# frozen_string_literal: true

module App
  class ExecutionTimer
    def initialize(app)
      @app = app
    end

    def call(env)
      start_time = Time.now

      status, headers, body = @app.call(env)

      finish_time = Time.now
      exec_time_ms = (finish_time - start_time) * 1_000_000

      [status, headers, "#{exec_time_ms} - #{body}"]
    end
  end
end
