# frozen_string_literal: true

# BEGIN
def reverse(str)
  reversed_symbols = []

  i = 0
  length = str.length

  while i <= (length / 2)
    reversed_symbols[i] = str[length - 1 - i]
    reversed_symbols[length - 1 - i] = str[i]

    i += 1
  end

  reversed_symbols.join
end
# END
