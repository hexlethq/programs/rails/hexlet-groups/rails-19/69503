# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  return '' if start > stop

  res = []
  i = start

  while i <= stop
    res << prepare_element(i)
    i += 1
  end

  res.join(' ')
end

def prepare_element(number)
  if (number % 3).zero? && (number % 5).zero?
    'FizzBuzz'
  elsif (number % 3).zero?
    'Fizz'
  elsif (number % 5).zero?
    'Buzz'
  else
    number
  end
end
# END
