# frozen_string_literal: true

# BEGIN
def fibonacci(number)
  number.negative? ? nil : calc_fibonacci(number)
end

def calc_fibonacci(number)
  arr = [0, 1]
  i = 2

  while i < number
    arr[i] = arr[i - 1] + arr[i - 2]
    i += 1
  end

  arr[number - 1]
end
# END
